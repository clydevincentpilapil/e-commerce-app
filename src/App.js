import { useEffect, useState } from 'react'
import { Container } from 'react-bootstrap';
import {Navigate, BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes} from 'react-router-dom'

import AppNavbar from './components/AppNavbar'

//admin only
import Dashboard from './pages/Dashboard';
import AddProduct from './pages/AddProduct';
import EditProduct from './pages/EditProduct';
import SetToAdmin from './pages/SetToAdmin';

//all
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Checkout from './pages/Checkout';
import Orders from './pages/Orders';
import Profile from './pages/Profile';

//auth
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import ErrorPage from './pages/ErrorPage'

import UserContext, { UserProvider } from './UserContext';
import './App.css'

function App() {
  
  const [user, setUser]= useState({id: null, isAdmin: null})
  const unsetUser = ()=>{
    localStorage.clear()
  }
  useEffect(()=>{
    if(localStorage.getItem('token')){
      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
        headers: {"Authorization" : `Bearer ${localStorage.getItem('token')}`}
      })
      .then(res => res.json())
      .then(data => {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        })
      }
    },[])
    
    return (
      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>  
        <AppNavbar/>
        <Container fluid>
          <Routes>
            <Route path='/dashboard/:page' element={<Dashboard/>}/>

            <Route path='/users/profile' element={<Profile/>}/>
            <Route path='/products/new' element={<AddProduct/>}/>
            <Route path='/products/:productId/edit' element={<EditProduct/>}/>
            <Route path='/users/toAdmin' element={<SetToAdmin/>}/>

            <Route path='/products' element={<Products/>}/>
            <Route path='/products/:productId' element={<ProductView/>}/>
            <Route path='/checkout' element={<Checkout/>}/>
            <Route path='/orders' element={<Orders/>}/>

            <Route path='/register' element={<Register/>}/>
            <Route path='/login' element={<Login/>}/>
            <Route path='/logout' element={<Logout/>}/>
            
            <Route path='/'element={(user.id && user.isAdmin)?
              <Navigate to={'/dashboard'}/>:<Navigate to={'/products'}/>
            }/>
            <Route path='/dashboard' element={(user.id && user.isAdmin)?
              <Navigate to={`/dashboard/${'products'}`}/>:<Navigate to={'/products'}/>
            }/>
            <Route path='/users' element={(user.id)?
              <Navigate to={'/users/profile'}/>:<Navigate to={'/products'}/>
            }/>
            <Route path='*' element={<ErrorPage/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
