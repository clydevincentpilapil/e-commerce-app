import { useEffect, useState } from "react";
import { Card, Col, Row } from "react-bootstrap";

import ProductCard from './ProductCard'

export default function Hot(){
    const [hots, setHots] = useState([])

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/hot`)
            .then(res=>res.json())
            .then(data=> setHots(data))
    }, [])


    return(
        <Col md={10} className="bg-success text-light offset-md-1 rounded">
            <h1 className="text-center mt-4">🔥🔥🔥Our Hottest Items🔥🔥🔥</h1>
            <Row className="p-1 mb-2">
            {
                hots.map(element=>{
                    return (
                        <Col xs={12} md={4} key={element._id}>
                            <ProductCard product={element}/>
                        </Col>
                    )
                })
            }
            </Row>
        </Col>
    )
}