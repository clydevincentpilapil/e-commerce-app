import { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'


export default function AdminProduct(){
    const [products,setProducts] = useState([])

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data=>{
                if(data.length>0)
                    setProducts(data)
            })
    },[])

    function patcher(productId, action){
        let actionName
        if(['activate','deactivate'].includes(action)){
            actionName=(action==='deactivate')?'archive':'activate'
            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/${actionName}`, {
                method:'PATCH',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then(res => res.json())
                .then(data=>{
                    if(data.success === true){
                        Swal.fire({
                            icon: 'success',
                            title: `Product Successfully ${actionName.charAt(0).toUpperCase() + actionName.slice(1)}d`,
                            text: data.message
                        }).then((result) => {
                            if (result['isConfirmed']){
                                window.location.reload()
                            }
                          })
                    }else if(data.fail_message!==undefined){
                        Swal.fire({
                            icon: 'error',
                            title: `Product Wasn't ${actionName.charAt(0).toUpperCase() + actionName.slice(1)}d`,
                            text: data.fail_message
                        })
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Unexpected Error',
                            text: 'An unexpected error occurred.'
                        })
                    }
                })
        }else{
            Swal.fire({
                icon: 'error',
                title: 'Unexpected Error',
                text: 'An error occurred.'
              })
        }
    }

    return(
        (products.length>0)?
            <table className="table table-success table-striped table-bordered">
                <thead className='text-center'>
                    <tr>
                        <th>Name</th>
                        <th className='d-none d-sm-table-cell'>Description</th>
                        <th>Price (PHP)</th>
                        <th>For Sale</th>
                        <th colSpan={2}>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {products.map(element => {
                        return (
                            <tr key={element._id}>
                                <td><Link to={`/products/${element._id}`}>{element.name}</Link></td>
                                <td className='text-wrap d-none d-sm-table-cell'>{element.description}</td>
                                <td>{element.price.toLocaleString()}</td>
                                <td className='text-center'><img className='img-button' src={(element.isActive)?
                                require('../images/stock.png'):require('../images/nostock.png')
                                }/></td>
                                <td className='text-center'><Link to={`/products/${element._id}/edit`} state={element}><img className='img-button' src={require('../images/edit.png')}/></Link></td>
                                <td className='text-center'>
                                    {(element.isActive)?
                                        <img className='img-button' as={Button} onClick={()=>patcher(element._id, 'deactivate')} src={require('../images/deactivate.png')}/>
                                    :
                                        <img className='img-button' as={Button} onClick={()=>patcher(element._id, 'activate')} src={require('../images/activate.png')}/>
                                    }
                                </td>
                            </tr>      
                        )
                    })}
                </tbody> 
            </table>
        :
            <h5 className='text-center'>No products inside the database. Please add a product</h5>
    )
}