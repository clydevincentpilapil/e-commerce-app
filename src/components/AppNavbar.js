import { Fragment, useContext } from 'react'
import { Container, Navbar, Nav } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'

import UserContext from '../UserContext'

export default function AppNavbar(){
    const {user} = useContext(UserContext)
    return (
        <Navbar bg="success" expand="lg" variant='light'>
            <Container fluid bg="dark">
                <Navbar.Brand as={NavLink} to='/' className='text-white'>
                    <img src={require('../images/ctrlaltdelight.png')} alt="ctrl-alt-delight-logo.png" className='img-fluid ' style={{"maxHeight": "30px"}}/>
                    Ctrl+Alt+Delight
                </Navbar.Brand>
                <Navbar.Toggle aria-controls='navbarSupportedContent'/>
                <Navbar.Collapse id='navbarSupportedContent'>
                    <Nav className='ms-auto'>
                        {(user.id)?
                            <Fragment>
                                <Nav.Link as={NavLink} to='/users/profile' className='text-white'>Profile</Nav.Link>
                                <Nav.Link as={NavLink} to='/products' className='text-white'>Products</Nav.Link>
                                {user.isAdmin===true && <Nav.Link as={NavLink} to='/dashboard' className='text-white'>Dashboard</Nav.Link>}
                                {user.isAdmin===false && <Nav.Link as={NavLink} to='/checkout' className='text-white'>Cart</Nav.Link>}
                                {(user.isAdmin===false) && <Nav.Link as={NavLink} to='/orders' className='text-white'>Orders</Nav.Link>}
                                <Nav.Link as={NavLink} to='/logout' className='text-white'>Logout</Nav.Link>
                            </Fragment>
                            :
                            <Fragment>
                                <Nav.Link as={NavLink} to='/products' className='text-white'>Products</Nav.Link>
                                <Nav.Link as={NavLink} to='/login' className='text-white'>Login</Nav.Link>
                                <Nav.Link as={NavLink} to='/register' className='text-white'>Register</Nav.Link>
                            </Fragment>
                        }                        
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}