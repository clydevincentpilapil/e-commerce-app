import { Fragment } from 'react'
import {Card, Button, Row, Col} from 'react-bootstrap'
import { Link } from 'react-router-dom'

import '../App.css'

export default function ProductCard({product}){
    const {_id, name, description, price, pic} = product

    return(
        <Card className="m-2">
            <Row>
                {(product.count!=undefined)?
                    <Fragment>
                        <Col xs={4} md={12} xl={4}>
                            <img src={
                                (pic===undefined)?require('../images/nopic.png'):pic
                            } className="img-fluid rounded-start prodpic" alt={name}/>
                        </Col>
                        <Col xs={8} md={12} xl={8}>
                            <Card.Body>
                                <Card.Title className='w-100 text-truncate'>{name}</Card.Title>
                                <Card.Subtitle className='w-100 text-truncate mb-1'>{description}</Card.Subtitle>
                                    <Card.Text className='m-0'>Price: Php {price.toLocaleString()}</Card.Text>
                                    <Card.Text>Sold <span className='fw-bold'>{product.count}</span> times</Card.Text>
                                <Button as={Link} to={`/products/${_id}`} variant="success">View</Button>
                            </Card.Body>
                        </Col>
                    </Fragment>
                :
                    <Fragment>
                        <Col xs={4}>
                            <img src={
                                (pic===undefined)?require('../images/nopic.png'):pic
                            } className="img-fluid rounded-start prodpic" alt={name}/>
                        </Col>
                        <Col xs={8}>
                            <Card.Body>
                                <Card.Title className='w-100 text-truncate'>{name}</Card.Title>
                                <Card.Subtitle className='w-100 text-truncate mb-1'>{description}</Card.Subtitle>
                                {(product.count!=undefined)?
                                    <Fragment>
                                        <Card.Text className='m-0'>Price: Php {price.toLocaleString()}</Card.Text>
                                        <Card.Text>Sold <span className='fw-bold'>{product.count}</span> times</Card.Text>
                                    </Fragment>
                                :
                                    <Card.Text>Price: Php {price.toLocaleString()}</Card.Text>
                                }
                                <Button as={Link} to={`/products/${_id}`} variant="success">View</Button>
                            </Card.Body>
                        </Col>
                    </Fragment>
                }
            </Row>
        </Card>
    )
}