import { Fragment, useState } from 'react'
import {Card, Button, Modal} from 'react-bootstrap'


export default function OrderCard({order}){
    const {_id, purchasedOn, products,totalAmount} = order
    const date = new Date(purchasedOn)

    const [visibileElement, setVisibileElement] = useState(-1)

    function changeView(index){
        setVisibileElement(index)
    }

    // https://www.appsloveworld.com/reactjs/100/33/render-multiple-modals-correctly-with-map-in-react-bootstrap
    return(
        <Fragment>
            <Card className="m-2">
                <Card.Body>
                    <Card.Title className='w-100 text-truncate'>Ordered on {date.toLocaleString('en-PH', {month: 'short', day:'numeric', year:'numeric', hour12:true, hour:'numeric', minute:'numeric'})}</Card.Title>
                    <Card.Subtitle className='w-100 text-truncate'>{products.length} product{products.length!=1 && 's'} bought - {(products.reduce(function(tot,curr){return tot+=curr.quantity},0))} item{(products.reduce(function(tot,curr){return tot+=curr.quantity},0))!=1 && 's'}</Card.Subtitle>
                    <Card.Text>Total Cost: Php {totalAmount.toLocaleString()}</Card.Text>
                    <Button variant="success" data-toggle="modal" data-target={`#modal-${_id}`} onClick={()=>changeView(_id)}>View</Button>
                </Card.Body>
            </Card>
            <Modal show={visibileElement===_id} onHide={()=>changeView(-1)} id={`modal-${_id}`}>
                <Modal.Dialog className='text-dark'>
                    <Modal.Header closeButton>
                        <Modal.Title>Order on {date.toLocaleString('en-PH', {month: 'short', day:'numeric', year:'numeric', hour12:true, hour:'numeric', minute:'numeric'})}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h5>Ordered Products:</h5>
                        {
                            products.map(element=>{
                                return (
                                    <p key={element._id} className='m-0'>{element.quantity}x {element.productName}</p>
                                )
                            })
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant='secondary' onClick={()=>changeView(-1)}>Close</Button>
                    </Modal.Footer>
                </Modal.Dialog>
            </Modal>
        </Fragment>
    )
}