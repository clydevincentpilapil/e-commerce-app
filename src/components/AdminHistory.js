import { useEffect, useState } from 'react'
import { Accordion, Row, Col } from 'react-bootstrap'
import OrderCard from './OrderCard'
// import OrderHistory from './OrderHistory'


export default function AdminHistory(){
    const [orders,setOrders] = useState([])

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data=>{
                if(data.length>0)
                    setOrders(data)
            })
    },[])

    return(
        (orders.length>0)?
            <Accordion className='p-1'>
                {orders.map(element=>{
                    return (
                        <Accordion.Item eventKey={element._id} key={element.email}>
                            <Accordion.Header>Orders for &nbsp;<span className="fw-bold">{element.email}</span> &nbsp;({element.orderedProduct.length} order{element.orderedProduct.length!=1 && 's'})</Accordion.Header>
                            <Accordion.Body>
                                <Row>
                                    {element.orderedProduct.map((product, index)=>{
                                        return (
                                            <Col key={product._id} xs={12} md={6} xl={4} className={`${(element.orderedProduct.length%2==1 && index==element.orderedProduct.length-1)?'offset-md-3 ':''}${(index==element.orderedProduct.length-1)?(element.orderedProduct.length%3==1)?'offset-xl-4':'offset-lg-0':''}${(element.orderedProduct.length%3==2 && index==element.orderedProduct.length-2)?'offset-xl-2':''}`}>
                                                <OrderCard key={product._id} order={product}/>
                                            </Col>
                                        )
                                    })}
                                </Row>
                            </Accordion.Body>
                        </Accordion.Item>
                    )
                })}
            </Accordion>
        :
            <h5 className='text-center'>No orders inside the database.</h5>
    )
}