import { useContext, useEffect, useState } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
import { Navigate, useLocation, useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";


export default function EditProduct(){
    const {productId} = useParams()
    const {user} = useContext(UserContext)
    const navigate = useNavigate()
    
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [isActive, setIsActive] = useState(false)
    const [_id, set_Id] =useState('')

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(res=>res.json())
            .then(data=>{
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
                set_Id(data._id)
            })
    }, [productId])
    
    useEffect(()=>{
        setIsActive((name!=='' && name!==undefined && description!=='' && description!==undefined && price!==0  && price!==undefined && _id!=='' && _id!==undefined))
    }, [name, description, price, _id])


    function editProduct(e){
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
            .then(res => res.json())
            .then(data => {
                if(data.success === true){
                    Swal.fire({
                        icon: 'success',
                        title: 'Product Successfully Edited',
                        text: data.message
                      })
                    navigate('/dashboard')
                }else if(data.fail_message!==undefined){
                    Swal.fire({
                        icon: 'error',
                        title: "Product Wasn't Edited",
                        text: data.fail_message
                      })
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Unexpected Error',
                        text: 'An unexpected error occurred.'
                      })
                }
            })
        setName('')
        setDescription('')
        setPrice(0)
    }

    return (
        (user.id && user.isAdmin===true)?
            <Row className='m-3 text-light'>
                <Col xs={12} md={8} lg={6} className='bg-success p-3 rounded offset-md-2 offset-lg-2 offset-xl-3'>
                    <Form onSubmit={(e)=>editProduct(e)}>
                        <h3>Enter Product Details</h3>
                        <Form.Control
                            type="hidden"
                            value={_id}
                        ></Form.Control>
                        <Form.Group controlId='productName' className='pt-2'>
                            <Form.Label>Name*</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='Enter product name'
                                value={name}
                                onChange={e =>setName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId='description' className='pt-2'>
                            <Form.Label>Description*</Form.Label>
                            <Form.Control
                                as="textarea"
                                rows="5"
                                placeholder='Enter description'
                                value={description}
                                onChange={e =>setDescription(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId='productPrice' className='pt-2'>
                            <Form.Label>Price (PHP)*</Form.Label>
                            <Form.Control
                                type='number'
                                placeholder="Enter product price"
                                value={price}
                                onChange={e =>setPrice(e.target.value)}
                                required
                            />
                        </Form.Group>
                        {isActive?
                            <Button type='submit' id='submitBtn' className='mt-3 l-green text-dark' variant='success' >Submit</Button>
                            :
                            <Button variant='secondary' type='submit' id='submitBtn' className='mt-3 text-dark' disabled>Submit</Button>
                        }
                    </Form>
                </Col>
            </Row>
        :
            <Navigate to='/products'/>
    )
}