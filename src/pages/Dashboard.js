import { useContext } from 'react';
import { Navigate, Link, useParams } from 'react-router-dom';
import { Button } from 'react-bootstrap'

import UserContext from '../UserContext';
import AdminProduct from '../components/AdminProduct';
import AdminHistory from '../components/AdminHistory';

export default function Dashboard(){
    const {page} = useParams()
    const {user, setUser} = useContext(UserContext)
    
    return (
        (user && user.isAdmin===false)?
            <Navigate to={'/products'}/>
        :
        <div className='bg-success my-2 m-sm-3  rounded'>
            <div className='text-white text-center'>
                <h2 className='pt-3'>Admin Dashboard {page}</h2>

                <Button className=' m-2 l-green text-dark' variant='success' as={Link} to={'/products/new'}>Add New Product</Button>
                <Button className=' m-2 l-green text-dark' variant='success' as={Link} to={'/users/toAdmin'}>Set User to Admin</Button>
                {(page==='history') && <Button className=' m-2 l-green text-dark' variant='success' as={Link} to ={`/dashboard/${'products'}`}>Show Products Details</Button>}
                {page==='products' &&<Button className=' m-2 l-green text-dark' variant='success' as={Link} to ={`/dashboard/${'history'}`}>Show Order History</Button>}
            </div>
            <div className='p-2'>
                {(page==='products') && <AdminProduct/>}
                {(page==='history') && <AdminHistory/>}
            </div>
        </div>
    )
}