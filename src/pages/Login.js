import { useContext, useEffect, useState } from 'react'
import {Form, Button, Col, Row} from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'

import UserContext from '../UserContext'

export default function Login(){
    const {user, setUser} = useContext(UserContext)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)
    const [isEmailValid, setIsEmailValid] = useState(false)
    
    useEffect(()=>{
        setIsActive((email!=='' && password!==''))
    },[email, password])

    useEffect(()=>{
        // copied from https://www.w3resource.com/javascript/form/email-validation.php
        setIsEmailValid(((email!='' && (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)))))
    }, [email])

    // Function to simulate user registration
    function authenticate(e){
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {
                if(typeof data.access !== 'undefined'){
                    localStorage.setItem('token', data.access)
                    retrieveUserDetails(data.access)
                    Swal.fire({
                        icon: 'success',
                        title: 'Login Successful',
                        text: data.message
                      })
                }else if(data.fail_message!==undefined){
                    Swal.fire({
                        icon: 'error',
                        title: 'Login Failed',
                        text: data.fail_message
                      })
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Unexpected Error',
                        text: 'An unexpected error occurred.'
                      })
                }
            })
        setEmail('')
        setPassword('')
    }

    const retrieveUserDetails = (token) =>{
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            headers: {"Authorization" : `Bearer ${token}`}
        })
            .then(res => res.json())
            .then(data => {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin,
                    orders: (data.orderedProduct && data.orderedProduct.length>0)?data.orderedProduct.length:0
                })
            })
    }

    return(
        (user.id)?
            (user.isAdmin===true)?
                <Navigate to='/dashboard'/>
            :
                <Navigate to='/products'/>
        :
            <Row className='m-3 text-light'>
                <Col xs={12} md={8} lg={6} className='bg-success p-3 rounded offset-md-2 offset-lg-2 offset-xl-3'>
                    <Form onSubmit={(e)=>authenticate(e)}>
                        <h3>Enter your details</h3>
                        <Form.Group controlId='userEmail' className='pt-2'>
                            <Form.Label>Email Address*</Form.Label>
                            <Form.Control
                                type='email'
                                placeholder='Enter email'
                                value={email}
                                onChange={e =>setEmail(e.target.value)}
                                required
                            />
                        {(!isEmailValid && email!='') && (<Form.Text className='text-white'>Please input a valid Email</Form.Text>)}
                        </Form.Group>
                        <Form.Group controlId='password' className='pt-2'>
                            <Form.Label>Password*</Form.Label>
                            <Form.Control
                                type='password'
                                placeholder='Enter a password'
                                value={password}
                                onChange={e =>setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>
                        {isActive?
                            <Button type='submit' id='submitBtn' className='mt-3 l-green text-dark' variant='success'>Log In</Button>
                        :
                            <Button variant='secondary' type='submit' id='submitBtn' className='mt-3 text-dark' disabled>Log In</Button>
                        }
                    </Form>
                </Col>
            </Row>
        
    )
}