import { useContext, useEffect, useState } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";


export default function SetToAdmin(){
    const navigate = useNavigate()

    const {user} = useContext(UserContext)
    const [email, setEmail] = useState('')
    const [isEmailValid, setIsEmailValid] = useState(false)
    const [userData, setUserData] = useState([])
    const [validUser, setValidUser] = useState(false)
    const [message, setMessage] = useState('')    
    const [isActive, setIsActive] = useState(false)

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res=>res.json())
            .then(data => setUserData(data))
    }, [])
    
    useEffect(()=>{
        setIsActive((email!==''&& email!==undefined && isEmailValid===true && validUser===true))
    },[email, isEmailValid, validUser])

    useEffect(()=>{
        // copied from https://www.w3resource.com/javascript/form/email-validation.php
        setIsEmailValid(((email!='' && (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)))))
        const i = userData.findIndex(e => e.email === email)
        console.log('i', i)
        if (i > -1) {
            if(userData[i].isAdmin===false){
                setValidUser(true)
                setMessage('')
            }else{
                setValidUser(false)
                setMessage('This user is already an admin.')
            }
        }else{
            setValidUser(false)
            setMessage('User not found.')
        }
    }, [email])

    function setToAdmin(e){
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/setAsAdmin`, {
            method: 'PATCH',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({email: email})
        })
            .then(res => res.json())
            .then(data => {
                if(data.success === true){
                    Swal.fire({
                        icon: 'success',
                        title: 'User Successfully Set to Admin',
                        text: data.message
                    })
                    navigate('/dashboard')
                }else if(data.fail_message!==undefined){
                    Swal.fire({
                        icon: 'error',
                        title: "User Wasn't Set to Admin",
                        text: data.fail_message
                    })
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Unexpected Error',
                        text: 'An unexpected error occurred.'
                    })
                }
            })
        setEmail('')
        setValidUser(false)
        setIsActive(false)
    }

    return (
        (user.isAdmin===true)?
            <Row className='m-3 text-light'>
                <Col xs={12} md={8} lg={6} className='bg-success p-3 rounded offset-md-2 offset-lg-2 offset-xl-3'>
                    <Form onSubmit={(e)=>setToAdmin(e)}>
                        <h3 className="text-center">Enter User to Admin</h3>
                        <Form.Group controlId='userEmail' className='pt-2'>
                            <Form.Label>Email Address*</Form.Label>
                            <Form.Control
                                type='email'
                                placeholder='Enter email'
                                value={email}
                                onChange={e =>setEmail(e.target.value)}
                                required
                            />
                            {(!isEmailValid && email!='') && (<Form.Text className='text-white'>Please input a valid Email</Form.Text>)}
                            {(isEmailValid && email!='' && !validUser) && (<Form.Text className='text-white'>{message}</Form.Text>)}
                        </Form.Group>
                        {isActive?
                            <Button type='submit' id='submitBtn' className='mt-3 l-green text-dark' variant='success'>Set To Admin</Button>
                        :
                            <Button variant='secondary' type='submit' id='submitBtn' className='mt-3 text-dark' disabled>Set To Admin</Button>
                        }
                    </Form>
                </Col>
            </Row>
        :
            <Navigate to='/products'/>
    )
}