import { Fragment, useContext, useEffect, useState } from 'react'

import ProductCard from '../components/ProductCard'
import { Col, Row } from 'react-bootstrap'
import UserContext from '../UserContext'
import Hot from '../components/Hot'

export default function Products(){
    const {user} = useContext(UserContext)
    const [products,setProducts] = useState([])

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products`,{
            headers: {"Content-Type": "application/json"}
        })
            .then(res => res.json())
            .then(data=>{
                if(data.length>0){
                    setProducts(data.map((product, index) =>{
                        return (
                            <Col key={product._id} xs={12} md={6} xl={4} className={`${(data.length%2==1 && index==data.length-1)?'offset-md-3':''} ${(data.length%3==1 && index==data.length-1)?'offset-xl-4':''} ${(data.length%3==2 && index==data.length-2)?'offset-xl-2':''} ${(data.length%3==0 && index==data.length-1)?'offset-xl-0':''}`}>
                                <ProductCard  product={product}/>
                            </Col>
                        )
                    }))
                }
            })
    },[])
    
    return (
        <Row className="mt-3">
            <Hot/>
            {products}
        </Row>
    )
}