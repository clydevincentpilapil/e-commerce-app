import { useContext, useEffect, useState } from "react"
import { Button, Card, Col, Form, Modal, Row } from "react-bootstrap"
import { Navigate, useNavigate } from "react-router-dom"
import Swal from "sweetalert2"

import UserContext from "../UserContext"

export default function Profile(){
    const {user} = useContext(UserContext)
    const [details,setDetails] = useState({})
    const [oldPass, setOldPass] = useState('')
    const [pass1, setPass1] = useState('')
    const [pass2, setPass2] = useState('')
    const [isActive, setIsActive] = useState(false)
    const [visibileElement, setVisibileElement] = useState(-1)
    const navigate=useNavigate()

    useEffect(()=>{
        setIsActive((oldPass!=='' && pass1!=='' && pass2!=='') && (pass1===pass2) && (oldPass!==pass1))
    },[oldPass, pass1, pass2])

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
            headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
        })
            .then(res=>res.json())
            .then(data=>{
                setDetails(data)
            })
    }, [])

    useEffect(()=>{
        if(visibileElement!==details._id){
            setOldPass('')
            setPass1('')
            setPass2('')
        }
    }, [visibileElement])

    function changeView(index){
        setVisibileElement(index)
    }

    function changePassword(e){
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/changePassword`, {
            method: 'PATCH',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                oldPass: oldPass,
                newPass: pass1
            })
        })
            .then(res => res.json())
            .then(data => {                
                if(data.success){ //success
                    Swal.fire({
                        icon: 'success',
                        title: 'Password Changed Successfully',
                        text: data.message
                    })
                    navigate('/logout')
                }else if(data.fail_message!==undefined){
                    Swal.fire({
                        icon: 'error',
                        title: 'Password Change Failed',
                        text: data.fail_message
                    })
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Unexpected Error',
                        text: 'An unexpected error occurred.'
                    })
                }
            })
        setOldPass('')
        setPass1('')
        setPass2('')
    }

    return(
        (localStorage.getItem('token'))?
            <Card className="m-3 p-3 rounded">
                <Row>
                    <Col xs={12} md={4}>
                        <img src={
                            (details.pic===undefined)?
                                require('../images/nopic.png')
                            :
                                details.pic
                        } className="img-fluid rounded-circle userpic" alt={`${details.firstName} ${details.lastName}`}/>
                    </Col>
                    <Col xs={12} md={8}>
                        <Card.Body>
                            <Card.Title className='w-100'>Welcome {`${details.firstName} ${details.lastName}`}!</Card.Title>
                            <Card.Subtitle className='w-100'>Email: {details.email}</Card.Subtitle>
                            {user.isAdmin===false && <Card.Text>Orders: {(details.orderedProduct && details.orderedProduct.length)>0?details.orderedProduct.length:0}</Card.Text>}
                            <Button variant="success" className="mt-4" data-target={`#modal-${details._id}`} onClick={()=>changeView(details._id)}>Change Password</Button>
                        </Card.Body>
                        <Modal show={visibileElement===details._id} onHide={()=>changeView(-1)} id={`modal-${details._id}`}>
                            <Modal.Dialog className='text-dark w-100'>
                                <Modal.Header closeButton>
                                    <Modal.Title>Change your password</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Form onSubmit={(e)=>changePassword(e)}>
                                        <Form.Group controlId='oldPass' className='pt-2'>
                                            <Form.Label>Old Password*</Form.Label>
                                            <Form.Control
                                                type='password'
                                                placeholder='Enter your password'
                                                value={oldPass}
                                                onChange={e =>setOldPass(e.target.value)}
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group controlId='password1' className='pt-2'>
                                            <Form.Label>New Password*</Form.Label>
                                            <Form.Control
                                                type='password'
                                                placeholder='Enter your new password'
                                                value={pass1}
                                                onChange={e =>setPass1(e.target.value)}
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group controlId='password2' className='pt-2'>
                                            <Form.Label>Confirm New Password*</Form.Label>
                                            <Form.Control
                                                type='password'
                                                placeholder='Confirm your new password'
                                                value={pass2}
                                                onChange={e =>setPass2(e.target.value)}
                                                required
                                            />
                                        </Form.Group>
                                        {(oldPass!=='' && oldPass===pass1) && (<Form.Text className="d-block">New password cannot be the same as the old password.</Form.Text>)}
                                        {(pass1!=='' && pass2!='' && pass2!==pass1) && (<Form.Text className="d-block">New password does not match.</Form.Text>)}
                                        {isActive?
                                            <Button type='submit' id='submitBtn' className='mt-3' variant='success'>Change Password</Button>
                                        :
                                            <Button variant='secondary' type='submit' id='submitBtn' className='mt-3' disabled>Change Password</Button>
                                        }   
                                    </Form>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant='danger' onClick={()=>changeView(-1)}>Cancel</Button>
                                </Modal.Footer>
                            </Modal.Dialog>
                        </Modal>
                    </Col>
                </Row>
            </Card>
        :
            <Navigate to='/login'/>
    )
}