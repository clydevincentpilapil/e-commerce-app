import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ErrorPage(){
    return (
        <Container>
            <div className='mt-3 m-2 bg-success p-2 rounded'>
                <h1>Page not Found</h1>
                <p>Go back to <Link to="/">homepage</Link>.</p>
            </div>
        </Container>
    )
}