import { useContext, useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'

import OrderCard from '../components/OrderCard'
import UserContext from '../UserContext'

export default function Orders(){
    const {user} = useContext(UserContext)
    const [orders,setOrders] = useState([])
    const [message,setMessage] = useState('')

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`,
            {headers: {"Authorization": `Bearer ${localStorage.getItem('token')}`}}
        )
            .then(res => res.json())
            .then(data=>{
                if(data.orderedProduct.length>0){
                    setOrders(data.orderedProduct)
                }else{
                    console.log('You have no orders.')
                    setMessage('You have no orders.')
                }
            })
    },[])
    
    return (
        (user.id && !user.isAdmin)?
            message?
                <div className='bg-success my-2 m-sm-3 rounded p-3'>
                    <div className='text-white text-center'>
                        <h2 className=''>{message}</h2>
                    </div>
                </div>
            :
                <Row className="mt-3">
                    {orders.map((order, index) =>{
                        return (
                            <Col key={order._id} xs={12} md={6} xl={4} className={`${(orders.length%2==1 && index==orders.length-1)?'offset-md-3 ':''}${(index==orders.length-1)?(orders.length%3==1)?'offset-xl-4':'offset-lg-0':''}${(orders.length%3==2 && index==orders.length-2)?'offset-xl-2':''}`}>
                                <OrderCard  order={order}/>
                            </Col>
                        )
                    })}
                </Row>
        :
            <Navigate to={'/products'}/>
    )
}