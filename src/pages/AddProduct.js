import { useContext, useEffect, useState } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";


export default function AddProduct(){
    const {user} = useContext(UserContext)
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0)
    const [isActive, setIsActive] = useState(false)
    
    useEffect(()=>{
        setIsActive((name!=='' && name!==undefined && description!=='' && description!==undefined && price!==0))
    }, [name, description, price])

    function addProduct(e){
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
            .then(res => res.json())
            .then(data => {
                if(data.success === true){
                    Swal.fire({
                        icon: 'success',
                        title: 'Product Successfully Added',
                        text: data.message
                      })
                }else if(data.fail_message!==undefined){
                    Swal.fire({
                        icon: 'error',
                        title: "Product Wasn't Added",
                        text: data.fail_message
                      })
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Unexpected Error',
                        text: 'An unexpected error occurred.'
                      })
                }
            })
        setName('')
        setDescription('')
        setPrice(0)
    }

    return (
        (user.id && user.isAdmin===true)?
            <Row className='m-3 text-light'>
                <Col xs={12} md={8} lg={6} className='bg-success p-3 rounded offset-md-2 offset-lg-2 offset-xl-3'>
                    <Form onSubmit={(e)=>addProduct(e)}>
                        <h3 className="text-center">Enter Product Details</h3>
                        <Form.Group controlId='productName' className='pt-2'>
                            <Form.Label>Name*</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='Enter product name'
                                value={name}
                                onChange={e =>setName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId='description' className='pt-2'>
                            <Form.Label>Description*</Form.Label>
                            <Form.Control
                                as="textarea"
                                rows="5"
                                placeholder='Enter description'
                                value={description}
                                onChange={e =>setDescription(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId='productPrice' className='pt-2'>
                            <Form.Label>Price (PHP)*</Form.Label>
                            <Form.Control
                                type='number'
                                placeholder="Enter product price"
                                value={price}
                                onChange={e =>setPrice(e.target.value)}
                                required
                            />
                        </Form.Group>
                        {isActive?
                            <Button type='submit' id='submitBtn' className='mt-3 l-green text-dark' variant='success' >Create</Button>
                        :
                            <Button variant='secondary' type='submit' id='submitBtn' className='mt-3 text-dark' disabled>Create</Button>
                        }
                    </Form>
                </Col>
            </Row>
        :
            <Navigate to='/products'/>
    )
}