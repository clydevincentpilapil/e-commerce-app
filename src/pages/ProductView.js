import { Fragment, useContext, useEffect, useState } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from  'sweetalert2'

import UserContext from "../UserContext";

export default function ProductView(){
    const {productId} = useParams()
    const {user} = useContext(UserContext)
    const navigate = useNavigate()

    const [name,setName] = useState('')
    const [description,setDescription] = useState('')
    const [price,setPrice] = useState(0)
    const [isActive, setIsActive] = useState(true)
    const [pic,setPic] = useState(undefined)
    
    const [quantity, setQuantity] = useState(1)
    const [style,setStyle] = useState({visibility:'visible'})
    const [isButtonActive, setIsButtonActive] = useState(false)
    const [message, setMessage] = useState('')

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(res=>res.json())
            .then(data=>{
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
                setIsActive(data.isActive)
                setPic(data.pic)
            })

    }, [productId])

    useEffect(()=>{
        if(quantity==='' || isNaN(quantity)){
            setIsButtonActive(false)
            setMessage('Quantity cannot be empty.')
        }else if(quantity<=0){
            setStyle({visibility:'hidden'})
            if(quantity>0 && quantity!=='-0'){
                setIsButtonActive(true)
            }else{
                (quantity===0)?setMessage('Quantity cannot be zero.'):setMessage('Quantity cannot be negative.')
                setIsButtonActive(false)
            }
        }else{
            setStyle({visibility:'visible'})
            setIsButtonActive(true)
            setMessage('')
        }
    }, [quantity])

    function changeQuantity(adjust=null, set=null){
        if(set==null){
            if(isNaN(quantity)){
                setQuantity(Number(1))
            }else{
                setQuantity(parseInt(quantity)+parseInt(adjust))
            }
        }else{
            setQuantity(parseInt(set))
        }
    }

    function addToCart(id){
        if(localStorage.getItem('cart')){
            let cart = JSON.parse(localStorage.getItem('cart'))
            // https://stackoverflow.com/questions/8217419/how-to-determine-if-javascript-array-contains-an-object-with-an-attribute-that-e
            const i = cart.findIndex(e => e.productId === id)
            if (i > -1) {
                cart[i].quantity+=quantity
                localStorage.setItem('cart', JSON.stringify(cart))
                Swal.fire({
                    icon: 'success',
                    title: 'Item already in Cart',
                    text: `Quantity has been increased by ${quantity}`
                })
            }else{
                cart.push({productId: id,productName:name,price:price, quantity: quantity})
                localStorage.setItem('cart', JSON.stringify(cart))
                Swal.fire({
                    icon: 'success',
                    title: 'Added To Cart'
                })
            }
        }else{
            localStorage.setItem('cart', JSON.stringify([{productId: id, productName:name, price:price, quantity: quantity}]))
            Swal.fire({
                icon: 'success',
                title: 'Added To Cart'
            })
        }
        navigate('/products')
    }

    // https://codepen.io/anitaparmar26/details/BaLYMeN
    return(
        <Row>
            <Col xs={12} sm={10} lg={8} className="offset-sm-1 offset-lg-2" >
                <Card className="m-3">
                    <Row>
                        <Col xs={12} md={4} className="d-flex justify-content-center">
                            <img src={(pic===undefined)?require('../images/nopic.png'):pic} className="img-fluid rounded-start prodpic" alt={name}/>
                        </Col>
                        <Col xs={12} md={8}>
                            <Card.Body>
                                <Card.Title className='w-100'>{name}</Card.Title>
                                <Card.Subtitle className='w-100'>{description}</Card.Subtitle>
                                <Card.Text>Price: Php {price}</Card.Text>
                                {(isActive)?
                                        (user.id)?
                                            (user.isAdmin===false)?
                                                <Fragment>
                                                    <Card.Text className="input-group">
                                                        <input type="button" value="-" onClick={()=>changeQuantity(-1)} className="button-minus border rounded-circle  iconic mx-1" style={style}/>
                                                        <input type="number" value={quantity} min="1" className="border-0 text-center w-25" onChange={e =>changeQuantity(undefined,e.target.value)}/>
                                                        <input type="button" value="+" onClick={()=>changeQuantity(1)} className="button-plus border rounded-circle iconic"/>
                                                    </Card.Text>
                                                    {(message!=='') && <Card.Text>{message}</Card.Text>}
                                                    {(isButtonActive)?
                                                        <Button className="mx-1" variant="success" onClick={()=>addToCart(productId, name, price)}>Add To Cart</Button>
                                                    :
                                                        <Button className="mx-1" variant="secondary" disabled>Add To Cart</Button>
                                                    }
                                                </Fragment>
                                            :
                                                <Card.Text className="text-danger">Purchases cannot be made with an admin account</Card.Text>
                                        :
                                            <Link className="btn btn-danger btn-block" to={'/login'}>Log In to Buy</Link>
                                :
                                    <Card.Text className="fst-italic">This item is unavailable for purchase.</Card.Text>
                                }
                            </Card.Body>
                        </Col>
                    </Row>
                </Card>
            </Col>
        </Row>
    )
}