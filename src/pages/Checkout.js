import { Fragment, useContext, useEffect, useState } from 'react';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Checkout(){
    const {user} = useContext(UserContext)
    const [products,setProducts] = useState([])
    const [total, setTotal] = useState(0)
    const [nQuantity, setNQuantity] = useState(1)
    const [canOrder, setCanOrder] = useState(true)
    const navigate = useNavigate()

    useEffect(()=>{
        if(localStorage.getItem('cart')){
            let cart = JSON.parse(localStorage.getItem('cart'))
            setProducts(cart)
            setTotal(cart.reduce(function(tot, cur,){return tot+=(cur.price*cur.quantity)},0))
        }      
    },[])

    useEffect(()=>{
        setCanOrder(!products.some((element)=>{
            return (element.quantity<0)
        }))
    }, [products])

    function order(){
        let cart = JSON.parse(localStorage.getItem('cart'))
        for (let i = 0; i < cart.length; ) {
            if(cart[i].quantity===0){
                cart.splice(i,1)
            }else{
                i++
            }
        }
        if(cart.length>0){
            fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body:JSON.stringify({products: cart})
            })
                .then(res=>res.json())
                .then(data=>{
                    if(data.success === true){
                        Swal.fire({
                            icon: 'success',
                            title: `Product${(products.length>1)?'s':''} Successfully Ordered`,
                            text: data.message
                        })
                        setProducts([])
                        localStorage.removeItem('cart')
                        navigate('/')
                    }else if(data.fail_message!==undefined){
                        Swal.fire({
                            icon: 'error',
                            title: `Product${(products.length>1)?'s Were':' Was'}n't Ordered`,
                            text: data.fail_message
                        })
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Unexpected Error',
                            text: 'An unexpected error occurred.'
                        })
                    }
                })
        }else{
            Swal.fire({
                icon: 'info',
                title: 'Cart is empty',
              })
        }
    }

    function adjustQuantity(newQuantity,set=null, id){
        let cart = JSON.parse(localStorage.getItem('cart'))
        const i = cart.findIndex(e => e.productId === id)
        if (i > -1) {
            if(set==null){
                if(isNaN(cart[i].quantity)){
                    cart[i].quantity=1
                }else{
                    cart[i].quantity=parseInt(cart[i].quantity)+parseInt(newQuantity)
                }
            }else{
                if(set<0){
                    set=0
                }
                cart[i].quantity=set
            }
            setNQuantity(cart[i].quantity)
            localStorage.setItem('cart', JSON.stringify(cart))
            setProducts(cart)
            setTotal(cart.reduce(function(tot, cur,){return tot+=(cur.price*cur.quantity)},0))
        }
    }

    function removeElem(id){
        let cart = JSON.parse(localStorage.getItem('cart'))
        const i = cart.findIndex(e => e.productId === id)
        if (i > -1) {
            cart.splice(i,1)
            localStorage.setItem('cart', JSON.stringify(cart))
            setProducts(cart)
            setTotal(cart.reduce(function(tot, cur,){return tot+=(cur.price*cur.quantity)},0))
            if(cart.length==0){
                localStorage.removeItem('cart')
                Swal.fire({
                    icon: 'info',
                    title: 'Cart has been emptied',
                    text: 'Redirecting to Products page.'
                  })
                navigate('/products')
            }
        }
    }

    return (
        (user.isAdmin===false)?
            <div className='bg-success my-2 m-sm-3 rounded p-3'>
                <div className='text-white text-center'>
                    <h2 className=''>Checkout</h2>
                    {localStorage.getItem('cart')?
                        <Fragment>
                            <h5>Please review your items before proceeding.</h5>
                            <h6 className='fst-italic fw-normal'>Please note that items in the cart will be lost upon logging out.</h6>
                            <table className="table table-success table-striped table-bordered">
                                <thead className='text-center'>
                                    <tr>
                                        <th>Name</th>
                                        <th>Price (PHP)</th>
                                        <th>Quantity</th>
                                        <th>Subtotal (PHP)</th>
                                        <th>Remove</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {products.map(element => {
                                        return (
                                            <tr key={element.productId}>
                                                <td>{element.productName}</td>
                                                <td>{element.price.toLocaleString()}</td>
                                                <td>
                                                    {element.quantity>0?
                                                        <input type="button" value="-" onClick={()=>adjustQuantity(-1,undefined, element.productId)} className="button-minus border rounded-circle iconic mx-1"/>
                                                    :
                                                        <input type="button" value="-" onClick={()=>adjustQuantity(-1,undefined, element.productId)} className="button-minus border rounded-circle iconic mx-1" style={{visibility:'hidden'}}/>
                                                    }
                                                <input type="number" value={element.quantity} min="0" className="border-0 text-center w-25" onChange={e =>adjustQuantity(undefined,e.target.value, element.productId)}/>
                                                <input type="button" value="+" onClick={()=>adjustQuantity(1, undefined, element.productId)} className="button-plus border rounded-circle iconic mx-1"/>
                                                </td>
                                                <td>{(element.quantity * element.price).toLocaleString()}</td>
                                                <td className='text-center'>
                                                    <img className='img-button' as={Button} onClick={()=>removeElem(element.productId)} src={require('../images/delete.png')}/>
                                        </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                                <tfoot>
                                    <tr className='fw-bold'>
                                        <td colSpan={3} className='text-end'>Total:</td>
                                        <td colSpan={2}>{total.toLocaleString()}</td>
                                    </tr>
                                </tfoot>
                            </table>
                            {(products.some(e=>e.quantity===0)) && <h6 className='fst-italic fw-normal py-1'>Orders with 0 quantity will automatically be removed.</h6>}
                            {(canOrder)?
                                <Button variant='success' className='l-green text-dark' onClick={()=>order()}>Confirm Checkout</Button>
                            :
                                <Fragment>
                                    <h6 className='fst-italic fw-normal'>Orders cannot have negative quantities.</h6>
                                    <Button variant='secondary' disabled>Confirm Checkout</Button>
                                </Fragment>
                            }
                        </Fragment>
                    :
                        <h5>No products to checkout. Please add an item to cart.</h5>
                    }
                </div>
            </div>
        :
            <Navigate to={'/products'}/>
    )
}