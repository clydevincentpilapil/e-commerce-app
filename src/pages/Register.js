import { useContext, useEffect, useState } from 'react'
import {Form, Button, Row, Col} from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

import UserContext from '../UserContext'
import '../App.css'

export default function Register(){

    const {user, setUser} = useContext(UserContext)
    const navigate = useNavigate()

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [pass1, setPass1] = useState('')
    const [pass2, setPass2] = useState('')
    const [isActive, setIsActive] = useState(false)
    const [isEmailValid, setIsEmailValid] = useState(false)
    
    useEffect(()=>{
        if ((firstName!=='' && lastName!=='' && email!=='' && pass1!=='' && pass2!=='' && isEmailValid===true) && (pass1===pass2)){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    },[firstName, lastName, email, pass1, pass2, isEmailValid])

    useEffect(()=>{
        // copied from https://www.w3resource.com/javascript/form/email-validation.php
        if(email!='' && (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
            setIsEmailValid(true)  
        }else{
            setIsEmailValid(false)
        }
    }, [email])

    function registerUser(e){
        e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: pass1
                })
        })
            .then(res => res.json())
            .then(regis_data => {
                // console.log(regis_data.message)                    
                if(regis_data.success){ //success
                    Swal.fire({
                        icon: 'success',
                        title: 'Registration Successful',
                        text: regis_data.message
                    })
                    navigate('/login')
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Registration Failed',
                        text: regis_data.fail_message
                    })
                }
            })
        setFirstName('')
        setLastName('')
        setEmail('')
        setPass1('')
        setPass2('')
    }

    return(
        (user.id)?
            <Navigate to='/login'/>
        :
            <Row className='m-3 text-white'>
                <Col xs={12} md={8} lg={6} className='offset-lg-1  p-3 bg-success rounded'>
                    <Form onSubmit={(e)=>registerUser(e)}>
                        <h3>Enter your details</h3>
                        <Form.Group controlId='userFirstName' className='pt-2'>
                            <Form.Label>First Name*</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='Enter your first name'
                                value={firstName}
                                onChange={e =>setFirstName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId='userLastName' className='pt-2'>
                            <Form.Label>Last Name*</Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='Enter your last name'
                                value={lastName}
                                onChange={e =>setLastName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId='userEmail' className='pt-2'>
                            <Form.Label>Email Address*</Form.Label>
                            <Form.Control
                                type='email'
                                placeholder='Enter email'
                                value={email}
                                onChange={e =>setEmail(e.target.value)}
                                required
                            />
                            {(!isEmailValid && email!='') && (<Form.Text className='text-white'>Please input a valid Email</Form.Text>)}
                        </Form.Group>
                        <Form.Group controlId='password1' className='pt-2'>
                            <Form.Label>Password*</Form.Label>
                            <Form.Control
                                type='password'
                                placeholder='Enter a password'
                                value={pass1}
                                onChange={e =>setPass1(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId='password2' className='pt-2'>
                            <Form.Label>Confirm Password*</Form.Label>
                            <Form.Control
                                type='password'
                                placeholder='Confirm your password'
                                value={pass2}
                                onChange={e =>setPass2(e.target.value)}
                                required
                            />
                            {(pass1!=='' && pass2!=='' && pass2!==pass1) && (<Form.Text className='text-white'>Passwords do not match.</Form.Text>)}
                        </Form.Group>
                        {isActive?
                            <Button type='submit' id='submitBtn' className='mt-3 l-green text-dark' variant='success'>Register</Button>
                        :
                            <Button variant='secondary' type='submit' id='submitBtn' className='mt-3 text-dark' disabled>Register</Button>
                        }                        
                    </Form>
                    <span className="d-block d-md-none  d-flex flex-column align-items-center">
                        <p>Already have an account?</p>
                        <Form>
                            <Button type='submit' variant='success' className='l-green text-dark'>Log In</Button>
                        </Form>
                    </span>
                </Col>
                <Col xs={12} md={4} lg={4} className='d-none d-md-block'>
                    <div className='p-3 bg-success rounded h-100 d-flex flex-column align-items-end'>
                        <p>Already have an account?</p>
                        <Button type='submit' variant='success' className='mt-1 l-green text-dark' onClick={()=>navigate('/login')}>Log In</Button>
                        <img src={require('../images/ctrlaltdelight.png')} alt="ctrl-alt-delight-logo.png" className='img-fluid'/>
                    </div>
                </Col>
            </Row>
    )
}